#ifndef SHAPEEDIT_H
#define SHAPEEDIT_H

#include <QWidget>
#include <QVector>
#include <QPainter>
#include <QDataStream>

class Shape
{
public:
    enum ShapeType {INIT = 1, POLY, RECT, STAR, ELLIP};

    /* init with the default value */
    Shape();
    /* init with a user defined value */
    Shape(ShapeType setShape);
    /* copy constructor */
    Shape(const Shape &shape);
    ~Shape();
    /* object assignment */
    Shape & operator=(const Shape & shape);
    /* object serialization */
    friend QDataStream & operator<<(QDataStream& stream, const QSharedPointer<Shape> shape);
    friend QDataStream & operator>>(QDataStream& stream, QSharedPointer<Shape> & shape);
    /* data of a Shape object */
    ShapeType shapeType;
    QVector<QPoint> shape_points;
};

class ShapeEdit : public QWidget
{
    Q_OBJECT
public:
    ShapeEdit(QWidget *parent = nullptr);
    /* virtual methods of the QWidget class */
    QSize minimumSizeHint() const override;
    QSize sizeHint() const override;
    /* getter method */
    bool isModified();
    /* setter method */
    void setModified(bool);
    /* reset the ShapeEdit object */
    void clear();
    void open();
    /* data of the ShapeEdit object */
    QList<QSharedPointer<Shape>> shapes;

signals:
    void dispModified();

    /* interface methods connected with the associated class mainwindow */
public slots:
    void createPoly();
    void createRect();
    void createEllip();
    void createStar();
    void zoomPainter();
    void panPainter();

    /* virtual event handler methods of the QWidget class */
protected:
    void paintEvent(QPaintEvent *event) override;
    void enterEvent(QEvent *event) override;
    void leaveEvent(QEvent *event) override;
    void mousePressEvent(QMouseEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *event) override;
    void mouseDoubleClickEvent(QMouseEvent *event) override;
    void keyPressEvent(QKeyEvent *event) override;
    void keyReleaseEvent(QKeyEvent *event) override;

private slots:
    void updateTimeout();

private:
    enum ShapeState { START = 1, BEGIN, COMPLETE_CTRL, COMPLETE_DISP};

    void drawShape();
    bool modified;
    bool display;
    bool ctrlkey;
    QPoint cur_pos;
    /* stores supported event handlers of a shape */
    quint8 ctrlState;
    /* stores the state of a shape during the creation phase */
    ShapeState shapeState;
    QPainter painter;
    /* stores a current shape in the creation phase */
    QSharedPointer<Shape> cur_shape;
    /* periodic callback to display all shapes on the painter */
    QTimer *updateTimer;
};

#endif // SHAPEEDIT_H
