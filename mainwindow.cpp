#include <QtWidgets>
#include "mainwindow.h"
#include "shapeedit.h"

#define APPLICATION "Create Shapes"

MainWindow::MainWindow()
    : shapeEdit(new ShapeEdit)
{
    setCentralWidget(shapeEdit);

    createFileMenu();
    createDrawMenu();

    connect(shapeEdit, &ShapeEdit::dispModified, this, &MainWindow::dispIsModified);

    setCurrentFile(QString());
}

void MainWindow::dispIsModified()
{
    setWindowModified(shapeEdit->isModified());
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    if (maybeSave()) {
        event->accept();
    } else {
        event->ignore();
    }
}

bool MainWindow::maybeSave()
{
    if (!shapeEdit->isModified()) {
        return true;
    }
    const QMessageBox::StandardButton ret
        = QMessageBox::warning(this, tr(APPLICATION),
                               tr("The file has been modified.\n"
                                  "Do you want to save your changes?"),
                               QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
    switch (ret) {
    case QMessageBox::Save:
        return save();
    case QMessageBox::Cancel:
        return false;
    default:
        break;
    }
    return true;
}

void MainWindow::newFile()
{
    if (maybeSave()) {
        shapeEdit->clear();
        setCurrentFile(QString());
    }
}

void MainWindow::setCurrentFile(const QString &fileName)
{
    curFile = fileName;
    shapeEdit->setModified(false);
    setWindowModified(false);

    QString shownName = curFile;
    if (curFile.isEmpty()) {
        shownName = "untitled.txt";
    }
    setWindowFilePath(shownName);
}

void MainWindow::open()
{
    if(maybeSave()) {
        QString fileName = QFileDialog::getOpenFileName(this);
        if (!fileName.isEmpty()) {
            loadFile(fileName);
        }
    }
}

void MainWindow::loadFile(const QString &fileName)
{
    QFile file(fileName);
    if (!file.open(QIODevice::ReadOnly)) {
        QMessageBox::warning(this, tr(APPLICATION),
                             tr("Cannot read file %1:\n%2.")
                             .arg(QDir::toNativeSeparators(fileName), file.errorString()));
        return;
    }

    QDataStream in(&file);

    shapeEdit->clear();
    in >> shapeEdit->shapes;
    setCurrentFile(fileName);
    shapeEdit->open();
}

bool MainWindow::save()
{
    if (curFile.isEmpty()) {
        return saveAs();
    } else {
        return saveFile(curFile);
    }
}

bool MainWindow::saveFile(const QString &fileName)
{
    QString errorMessage;

    QSaveFile file(fileName);
    if (file.open(QIODevice::WriteOnly)) {
        QDataStream out(&file);
        out << shapeEdit->shapes;
        if (!file.commit()) {
            errorMessage = tr("Cannot write file %1:\n%2.")
                           .arg(QDir::toNativeSeparators(fileName), file.errorString());
        }
    } else {
        errorMessage = tr("Cannot open file %1 for writing:\n%2.")
                       .arg(QDir::toNativeSeparators(fileName), file.errorString());
    }

    if (!errorMessage.isEmpty()) {
        QMessageBox::warning(this, tr(APPLICATION), errorMessage);
        return false;
    }

    setCurrentFile(fileName);
    return true;
}

bool MainWindow::saveAs()
{
    QFileDialog dialog(this);
    dialog.setWindowModality(Qt::WindowModal);
    dialog.setAcceptMode(QFileDialog::AcceptSave);
    if (dialog.exec() != QDialog::Accepted) {
        return false;
    }
    return saveFile(dialog.selectedFiles().first());
}

void MainWindow::info()
{
   QMessageBox::information(this, tr(APPLICATION),
            tr("Draw shapes project."));
}

void MainWindow::createFileMenu()
{
    /* create menu */
    QMenu *fileMenu = menuBar()->addMenu(tr("File"));

    /* create toolbar */
    QToolBar *fileToolBar = addToolBar(tr("File"));

    /* create icons */
    const QIcon newIcon = QIcon::fromTheme("new", QIcon(":/images/new.png"));
    const QIcon openIcon = QIcon::fromTheme("open", QIcon(":/images/open.png"));
    const QIcon saveIcon = QIcon::fromTheme("save", QIcon(":/images/save.png"));
    const QIcon saveAsIcon = QIcon::fromTheme("saveas", QIcon(":/images/saveas.png"));
    const QIcon infoIcon = QIcon::fromTheme("info", QIcon(":/images/info.png"));
    const QIcon exitIcon = QIcon::fromTheme("application-exit");

    /* create actions */
    QAction *newAct = new QAction(newIcon, tr("New"), this);
    QAction *openAct = new QAction(openIcon, tr("Open"), this);
    QAction *saveAct = new QAction(saveIcon, tr("Save"), this);
    QAction *saveAsAct = new QAction(saveAsIcon, tr("Save As"), this);
    QAction *infoAct = new QAction(infoIcon, tr("Info"), this);
    QAction *exitAct = new QAction(exitIcon, tr("Exit"), this);

    /* set callbacks of actions */
    connect(newAct, &QAction::triggered, this, &MainWindow::newFile);
    connect(openAct, &QAction::triggered, this, &MainWindow::open);
    connect(saveAct, &QAction::triggered, this, &MainWindow::save);
    connect(saveAsAct, &QAction::triggered, this, &MainWindow::saveAs);
    connect(infoAct, &QAction::triggered, this, &MainWindow::info);
    connect(exitAct, &QAction::triggered, this, &QWidget::close);

    /* add actions to menu */
    fileMenu->addAction(newAct);
    fileMenu->addAction(openAct);
    fileMenu->addSeparator();
    fileMenu->addAction(saveAct);
    fileMenu->addAction(saveAsAct);
    fileMenu->addSeparator();
    fileMenu->addAction(exitAct);

    /* add actions to toolbar */
    fileToolBar->addAction(newAct);
    fileToolBar->addSeparator();
    fileToolBar->addAction(openAct);
    fileToolBar->addSeparator();
    fileToolBar->addAction(saveAct);
    fileToolBar->addAction(saveAsAct);
    fileToolBar->addSeparator();
    fileToolBar->addAction(infoAct);
    fileToolBar->addAction(exitAct);

    menuBar()->addSeparator();

    /* create help menu */
    QMenu *helpMenu = menuBar()->addMenu(tr("&Help"));
    helpMenu->addAction(infoAct);
}

void MainWindow::createDrawMenu()
{
    /* create toolbar */
    QToolBar *drawToolBar = addToolBar(tr("Draw"));
    addToolBar(Qt::LeftToolBarArea, drawToolBar);

    /* create icons */
    const QIcon polyIcon = QIcon::fromTheme("polygon", QIcon(":/images/polygon.png"));
    const QIcon rectIcon = QIcon::fromTheme("rectangle", QIcon(":/images/rectangle.png"));
    const QIcon ellipIcon = QIcon::fromTheme("ellipse", QIcon(":/images/ellipse.png"));
    const QIcon starIcon = QIcon::fromTheme("star", QIcon(":/images/star.png"));
    const QIcon zoomIcon = QIcon::fromTheme("zoom", QIcon(":/images/zoom.png"));
    const QIcon panIcon = QIcon::fromTheme("pan", QIcon(":/images/pan.png"));

    /* create actions */
    QAction *polyAct = new QAction(polyIcon, tr("Polygon"), this);
    QAction *rectAct = new QAction(rectIcon, tr("Rectangle"), this);
    QAction *ellipAct = new QAction(ellipIcon, tr("Ellipse"), this);
    QAction *starAct = new QAction(starIcon, tr("Star"), this);
    QAction *zoomAct = new QAction(zoomIcon, tr("Zoom"), this);
    QAction *panAct = new QAction(panIcon, tr("Pan"), this);

    /* set callbacks of actions */
    connect(polyAct, &QAction::triggered, shapeEdit, &ShapeEdit::createPoly);
    connect(rectAct, &QAction::triggered, shapeEdit, &ShapeEdit::createRect);
    connect(ellipAct, &QAction::triggered, shapeEdit, &ShapeEdit::createEllip);
    connect(starAct, &QAction::triggered, shapeEdit, &ShapeEdit::createStar);
    connect(zoomAct, &QAction::triggered, shapeEdit, &ShapeEdit::zoomPainter);
    connect(panAct, &QAction::triggered, shapeEdit, &ShapeEdit::panPainter);

    /* add actions to toolbar */
    drawToolBar->addAction(polyAct);
    drawToolBar->addAction(rectAct);
    drawToolBar->addAction(ellipAct);
    drawToolBar->addAction(starAct);
    drawToolBar->addSeparator();
    drawToolBar->addAction(zoomAct);
    drawToolBar->addAction(panAct);
}
