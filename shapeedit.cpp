#include <QTimer>
#include <QEvent>
#include <QMouseEvent>
#include <QKeyEvent>
#include <QPainter>
#include "shapeedit.h"
#include <QDebug>

/* constants used to associate supported event types for a new shape */
static const quint8 INIT = 0x0;
static const quint8 CLICK_PRESS = 0x1;
static const quint8 CLICK_RELEASE = 0x2;
static const quint8 DOUBLE_CLICK = 0x4;
static const quint8 KEYPRESS = 0x8;

/*** start Shape class ***/

/* constructor with the default shape type */
Shape::Shape()
{
    shapeType = INIT;
}

/* constructor with a user provided shape type */
Shape::Shape(ShapeType setShape)
{
    shapeType = setShape;
}

/* copy constructor to init a shape object from an existing shape */
Shape::Shape(const Shape &shape)
{
    shapeType = shape.shapeType;
    shape_points = shape.shape_points;
}

/* destructor to delete the dynamic object shape_points */
Shape::~Shape()
{
    shape_points.clear();
}

/* assign a right side shape object to a left side shape object */
Shape & Shape::operator=(const Shape &shape)
{
    if(this != &shape) {
        shapeType = shape.shapeType;
        shape_points = shape.shape_points;
    }
    return *this;
}

/* store a shape object as binary stream */
QDataStream &operator<<(QDataStream &ds, const QSharedPointer<Shape> shape)
{
    ds << shape->shape_points << shape->shapeType;
    return ds;
}

/* restore binary stream as a shape object */
QDataStream &operator>>(QDataStream &ds, QSharedPointer<Shape> & shape)
{
    /* assert reference to a pointer is null */
    Q_ASSERT(shape.isNull());
    /* allocate memory to a null pointer.
     * without the reference to a pointer the allocated memory
     * is not assigned to a shape pointer in the shapes pointers
     * list. */
    shape = QSharedPointer<Shape>(new Shape());
    ds >> shape->shape_points >> shape->shapeType;
    return ds;
}
/*** end Shape class ***/

/*
 *** Shape editor class ***
 */

ShapeEdit::ShapeEdit(QWidget *parent)
    : QWidget(parent)
{
    modified = false;
    display = false;
    ctrlkey = false;
    ctrlState = INIT;
    shapeState = START;
    cur_shape = NULL;

    setBackgroundRole(QPalette::Base);
    setAutoFillBackground(true);

    /* periodically draw all shape objects */
    updateTimer = new QTimer(this);
    updateTimer->setInterval(1);
    connect(updateTimer, &QTimer::timeout, this, &ShapeEdit::updateTimeout);
    updateTimer->start();

    /* enable keyboard events */
    setFocusPolicy(Qt::StrongFocus);
}

QSize ShapeEdit::minimumSizeHint() const
{
    return QSize(100, 100);
}

QSize ShapeEdit::sizeHint() const
{
    return QSize(500, 500);
}

/* getter method */
bool ShapeEdit::isModified()
{
    return modified;
}

/* setter method.
 * sends signal to the associated class mainwindow. */
void ShapeEdit::setModified(bool flag)
{
    if (modified != flag) {
        modified = flag;
        emit dispModified();
    }
}

/* callback when ShapeEdit QWidget has the mouse pointer */
void ShapeEdit::enterEvent(QEvent *event)
{
    display = true;
    event->accept();
}

/* callback when ShapeEdit QWidget is without mouse pointer */
void ShapeEdit::leaveEvent(QEvent *event)
{
    display = false;
    event->accept();
}

/* timer callback for the shapes draw loop */
void ShapeEdit::updateTimeout()
{
    /* trigger the paintEvent */
    update();
    if (display != true) {
        return;
    }
    if (shapeState == BEGIN) {
        cur_pos = this->mapFromGlobal(QCursor::pos());
    }
}

/* sets BEGIN state */
void ShapeEdit::mousePressEvent(QMouseEvent *event)
{
    if (!(ctrlState & CLICK_PRESS)) {
        return;
    }
    /* ignore mouse press and release in case of mouse double click */
    if (shapeState == COMPLETE_DISP) {
        return;
    }
    /* ignore mouse press if mouse position is same */
    if (cur_shape->shape_points.isEmpty() == false &&
        cur_shape->shape_points.last() == event->pos()) {
        return;
    }
    cur_shape->shape_points << event->pos();
    shapeState = BEGIN;
}

/* handles non POLY shape draw */
void ShapeEdit::mouseReleaseEvent(QMouseEvent *event)
{
    if (!(ctrlState & CLICK_RELEASE)) {
        return;
    }
    updateTimer->stop();
    if (shapeState == BEGIN) {
        /* ignore mouse press if mouse position is same */
        if (cur_shape->shape_points.last() == event->pos()) {
            cur_shape->shape_points.removeLast();
            /* reset shapeState */
            shapeState = START;
        } else {
            cur_shape->shape_points << event->pos();
            /* identify shape draw is complete */
            shapeState = COMPLETE_DISP;
        }
    }
    updateTimer->start();
}

/* handles POLY shape draw.
 * mouse press and release are called before and after the mouse double click.
 * COMPLETE_DISP state is used to identify the mouse double click. */
void ShapeEdit::mouseDoubleClickEvent(QMouseEvent *event)
{
    (void)event;
    if (!(ctrlState & DOUBLE_CLICK)) {
        return;
    }
    updateTimer->stop();
    if (shapeState == BEGIN) {
        /* atleast two points are required for a shape creation */
        if (cur_shape->shape_points.count() == 1) {
            cur_shape->shape_points.removeLast();
            /* reset shapeState */
            shapeState = START;
        } else {
            /* identify mouse double click is triggered.
             * identify shape draw is complete. */
            shapeState = COMPLETE_DISP;
        }
    }
    updateTimer->start();
}

/* handles non POLY shape draw */
void ShapeEdit::keyPressEvent(QKeyEvent *event)
{
    if (!(ctrlState & KEYPRESS)) {
        return;
    }
    if (shapeState == START) {
        return;
    }

    if (event->key() != Qt::Key_Control) {
        return;
    }
    ctrlkey = true;
}

void ShapeEdit::keyReleaseEvent(QKeyEvent *event)
{
    (void)event;
    ctrlkey = false;
}

/* reset the painter data */
void ShapeEdit::clear()
{
    setModified(false);
    ctrlkey = false;
    ctrlState = INIT;
    /* reset shapeState */
    shapeState = START;
    /* clears shapes and shape's dynamic data */
    shapes.clear();
    cur_shape = NULL;
    /* refresh the painter */
    update();
}

void ShapeEdit::open()
{
    cur_shape = shapes.last();
}

/* sets COMPLETE_CTRL state */
void ShapeEdit::createPoly()
{
    if (cur_shape == NULL) {
        /* add the new shape */
        shapes.append(QSharedPointer<Shape>(new Shape(Shape::POLY)));
        cur_shape = shapes.last();
    }
    if (2 <= shapes.count()) {
        setModified(true);
    }
    updateTimer->stop();
    if (shapeState == BEGIN || shapeState == COMPLETE_DISP) {
        if (cur_shape->shape_points.count() == 1) {
            cur_shape->shape_points.removeLast();
            /* reset shapeState */
            shapeState = START;
        } else {
            /* add the new shape */
            shapes.append(QSharedPointer<Shape>(new Shape(Shape::POLY)));
            /* override the COMPLETE_DISP state */
            shapeState = COMPLETE_CTRL;
        }
    }
    /* update the shape type if changed from an other shape */
    if (shapeState == START || shapeState == COMPLETE_CTRL) {
        shapes.last()->shapeType = Shape::POLY;
    }
    updateTimer->start();
    ctrlState = CLICK_PRESS|DOUBLE_CLICK;
}

/* sets COMPLETE_CTRL state */
void ShapeEdit::createRect()
{
    if (cur_shape == NULL) {
        /* add the new shape */
        shapes.append(QSharedPointer<Shape>(new Shape(Shape::RECT)));
        cur_shape = shapes.last();
    }
    if (2 <= shapes.count()) {
        setModified(true);
    }
    updateTimer->stop();
    if (shapeState == BEGIN || shapeState == COMPLETE_DISP) {
        if (cur_shape->shape_points.count() == 1) {
            cur_shape->shape_points.removeLast();
            /* reset shapeState */
            shapeState = START;
        } else {
            /* add the new shape */
            shapes.append(QSharedPointer<Shape>(new Shape(Shape::RECT)));
            /* overide the COMPLETE_DISP state */
            shapeState = COMPLETE_CTRL;
        }
    }
    /* update the shape type if changed from an other shape */
    if (shapeState == START || shapeState == COMPLETE_CTRL) {
        shapes.last()->shapeType = Shape::RECT;
    }
    updateTimer->start();
    ctrlState = CLICK_PRESS|CLICK_RELEASE|KEYPRESS;
}

/* sets COMPLETE_CTRL state */
void ShapeEdit::createEllip()
{
    if (cur_shape == NULL) {
        /* add the new shape */
        shapes.append(QSharedPointer<Shape>(new Shape(Shape::ELLIP)));
        cur_shape = shapes.last();
    }
    if (2 <= shapes.count()) {
        setModified(true);
    }
    updateTimer->stop();
    if (shapeState == BEGIN || shapeState == COMPLETE_DISP) {
        if (cur_shape->shape_points.count() == 1) {
            cur_shape->shape_points.removeLast();
            /* reset shapeState */
            shapeState = START;
        } else {
            /* add the new shape */
            shapes.append(QSharedPointer<Shape>(new Shape(Shape::ELLIP)));
            /* overide the COMPLETE_DISP state */
            shapeState = COMPLETE_CTRL;
        }
    }
    /* update the shape type if changed from an other shape */
    if (shapeState == START || shapeState == COMPLETE_CTRL) {
        shapes.last()->shapeType = Shape::ELLIP;
    }
    updateTimer->start();
    ctrlState = CLICK_PRESS|CLICK_RELEASE|KEYPRESS;
}

/* sets COMPLETE_CTRL state */
void ShapeEdit::createStar()
{
    if (cur_shape == NULL) {
        /* add the new shape */
        shapes.append(QSharedPointer<Shape>(new Shape(Shape::STAR)));
        cur_shape = shapes.last();
    }
    if (2 <= shapes.count()) {
        setModified(true);
    }
    updateTimer->stop();
    if (shapeState == BEGIN || shapeState == COMPLETE_DISP) {
        if (cur_shape->shape_points.count() == 1) {
            cur_shape->shape_points.removeLast();
            /* reset shapeState */
            shapeState = START;
        } else {
            /* add the new shape */
            shapes.append(QSharedPointer<Shape>(new Shape(Shape::STAR)));
            /* overide the COMPLETE_DISP state */
            shapeState = COMPLETE_CTRL;
        }
    }
    /* update the shape type if changed from an other shape */
    if (shapeState == START || shapeState == COMPLETE_CTRL) {
        shapes.last()->shapeType = Shape::STAR;
    }
    updateTimer->start();
    ctrlState = CLICK_PRESS|CLICK_RELEASE|KEYPRESS;
}

void ShapeEdit::zoomPainter()
{

}

void ShapeEdit::panPainter()
{

}

/* draw all shapes */
void ShapeEdit::drawShape()
{
    QRect rect;
    QListIterator<QSharedPointer<Shape>> iter(shapes);

    painter.begin(this);
    while(iter.hasNext())
    {
        const QSharedPointer<Shape> iter_shape = iter.next();
        if (iter_shape->shape_points.count() == 0) {
            break;
        }

        switch(iter_shape->shapeType) {
            case Shape::POLY:
                if (iter_shape == cur_shape) {
                    if (2 <= iter_shape->shape_points.count()) {
                        painter.setPen(QPen(Qt::black, 1, Qt::SolidLine, Qt::FlatCap,  Qt::MiterJoin));
                        painter.drawPolyline(iter_shape->shape_points);
                    }
                    painter.setPen(QPen(Qt::black, 1, Qt::DashLine, Qt::FlatCap,  Qt::MiterJoin));
                    painter.drawLine(iter_shape->shape_points.last(), cur_pos);
                } else {
                    painter.setPen(QPen(Qt::black, 1, Qt::SolidLine, Qt::FlatCap,  Qt::MiterJoin));
                    painter.drawPolygon(iter_shape->shape_points);
                }
                break;
            case Shape::RECT:
                if (iter_shape == cur_shape) {
                    painter.setPen(QPen(Qt::black, 1, Qt::DashLine, Qt::FlatCap,  Qt::MiterJoin));
                    rect = QRect(iter_shape->shape_points.first(), cur_pos);
                } else {
                    painter.setPen(QPen(Qt::black, 1, Qt::SolidLine, Qt::FlatCap,  Qt::MiterJoin));
                    rect = QRect(iter_shape->shape_points.first(), iter_shape->shape_points.last());
                }
                painter.drawRect(rect);
                break;
            case Shape::ELLIP:
                if (iter_shape == cur_shape) {
                    painter.setPen(QPen(Qt::black, 1, Qt::DashLine, Qt::FlatCap,  Qt::MiterJoin));
                    rect = QRect(iter_shape->shape_points.first(), cur_pos);
                } else {
                    painter.setPen(QPen(Qt::black, 1, Qt::SolidLine, Qt::FlatCap,  Qt::MiterJoin));
                    rect = QRect(iter_shape->shape_points.first(), iter_shape->shape_points.last());
                }
                painter.drawEllipse(rect);
                break;
            case Shape::STAR:
                break;
            default:
                break;
        }

        /* exit the iterator loop */
        if (iter_shape == cur_shape) {
            break;
        }
    }
    painter.end();
}

/* painter callback */
void ShapeEdit::paintEvent(QPaintEvent *event)
{
    (void)event;
    drawShape();
    if (display != true) {
        return;
    }
    /* execute after the mouse focus has shifted to the ShapeEdit
     * from a toolbar menu. */
    if (shapeState == COMPLETE_DISP) {
        /* add the new shape */
        shapes.append(QSharedPointer<Shape>(new Shape(cur_shape->shapeType)));
    }
    if (shapeState == COMPLETE_DISP || shapeState == COMPLETE_CTRL) {
        /* start the new shape draw */
        cur_shape = shapes.last();
        /* reset shapeState */
        shapeState = START;
    }
}

/*
 *** Shape editor class ***
 */
