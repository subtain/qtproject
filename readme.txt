This is a simple qt framework based c++ application to draw multiple shapes i.e.
polygon, ellipse and rectangle.
Drawn objects are saved in a files using object serialization that can be reloaded again.
Further, this c++ application demonstrates design of a state machine for human machine
interaction with controls in the graphical interface and usage of c++ concepts and
techniques.